#include <stdio.h>
#include <glib.h>

//Type definitions
typedef gint64 Number;

struct Range
{
	Number Beginning;
	Number End;
};

enum CousinProcessingType
{
	ProcessRange = 0,
	ProcessNumberOfCousins = 1
};

//Function declarations
Number ProcessCousins(Number cousins, struct Range range, guint8 processingType);
guint8 IsCousin(Number number);

//Commandline variables
gint64 beginning = 0;
gint64 end = 0;
gint64 cousins = 0;
gboolean processcousins = FALSE;
gboolean processrange = FALSE;

//Commandline entries
GOptionEntry entries[] =
{
	{ "beginning", 'b', 0, G_OPTION_ARG_INT64, &beginning, "Beggining of the range", "B" },
	{ "end", 'e', 0, G_OPTION_ARG_INT64, &end, "End of the range", "E" },
	{ "cousins", 'c', 0, G_OPTION_ARG_INT64, &cousins, "Cousin numbers to show", "C" },
	{ "processcousins", 'p', 0, G_OPTION_ARG_NONE, &processcousins, "Process cousin numbers", NULL },
	{ "processrange", 'r', 0, G_OPTION_ARG_NONE, &processrange, "Show cousin numbers between the range specified", NULL },
	{ NULL }
};

/* Main program entry */
int main(int argc, char *argv[])
{
	GOptionContext *context;

	context = g_option_context_new("- process cousins numbers");

	g_option_context_add_main_entries (context, entries, NULL);
	g_option_context_set_help_enabled(context, TRUE);

	if(g_option_context_parse(context, &argc, &argv, NULL) == TRUE)
	{
		struct Range range;

		if(((processrange == TRUE) && (processcousins == TRUE)) || ((processrange == FALSE) && (processcousins == FALSE)))
                {
                        printf("Please, choose a valid cousin processing type.\n");
			return -1;
                }

		if(((beginning < 1) || (end <= beginning)) && (processrange == TRUE))
		{
			printf("Please, choose a valid cousin range.\n");
			return -1;
		}

		if((cousins <= 0) && (processcousins == TRUE))
		{
			printf("Please, choose a valid cousins value.\n");
			return -1;
		}


		if(processrange == TRUE)
		{
			range.Beginning = beginning;
			range.End = end;

			printf("\033[1;31mNumber of cousins obtained:\033[1;37m %li\033[0m\n\n", ProcessCousins(0, range, ProcessRange));
		}
		else
		{
			printf("\033[1;31mNumber of cousins obtained:\033[1;37m %li\033[0m\n\n", ProcessCousins(cousins, range, ProcessNumberOfCousins));
		}
	}
	else
	{
		printf("%s", g_option_context_get_help(context, TRUE, NULL));
	}

	return 0;
}

/* Process the cousins specified */
Number ProcessCousins(Number cousins, struct Range range, guint8 processingType)
{
	Number i;
	Number a;

	printf("\033[1;34m<<<\033[1;33mList of Cousin numbers (here we go!! XD)\033[1;34m>>>\033[0m\n\n");

	switch(processingType)
	{
		case ProcessRange:
			for(i = range.Beginning, a = 0; i < range.End; i++)
        		{
                		if(IsCousin(i) == TRUE)
				{
                        		printf("\033[1;34m|\033[1;37m%li\033[0m", i);
					a++;
				}
			}

			printf("\033[1;34m|\033[0m");
			break;
		case ProcessNumberOfCousins:
			for(i = 1, a = 0; (i < G_MAXINT64) && (a < cousins); i++)
			{
				if(IsCousin(i) == TRUE)
				{
					printf("\033[1;34m|\033[1;37m%li\033[0m", i);
					a++;
				}
			}

			printf("\033[1;34m|\033[0m");
			break;
		default:
			printf("Process type unknown.\n");
			break;
	}

	printf("\033[1;31m\n\nList finished!! :P\033[0m\n\n");

	return a;
}

/* Checks if a number is cousin */
guint8 IsCousin(Number number)
{
	Number i;

	if((number == 1) || (number % 2 == 0) || (number % 5 == 0))
		return FALSE;

	for(i = 2; i < (number - 1); i++)
	{
		if((number % i) == 0)
			return FALSE;
	}

	return TRUE;
}
