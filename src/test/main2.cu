#include <stdio.h>
#include <limits.h>
#include <cuda_runtime_api.h>

#define TRUE 1
#define FALSE 0

__device__ int *gpuvalues;
int *cpuvalues;
#define MAXVALUES 102400

/*__device__ unsigned short IsCousin(int number)
{
	__shared__ int i;


        if(number == 1)
                return FALSE;

        for(i = 2; i < (number - 1); i++)
        {
                if((number % i) == 0)
                        return FALSE;
        }

        return TRUE;
}*/

__global__ void KernelCall(int *values)
{
//	__shared__ int i;
//	__shared__ int a;
	__shared__ int index;
	__shared__ int i;

	index = (((blockIdx.y * gridDim.x) + blockIdx.x) * (blockDim.x * blockDim.y)) + ((threadIdx.y * blockDim.x) + threadIdx.x);

//	printf("Thread y:%i x:%i\n", threadIdx.y, threadIdx.x);
//	printf("Thread index: %i\n", index);

/*	printf("Thread: Y:%i, X:%i\n", threadIdx.y, threadIdx.x);
	printf("Block: Y:%i, X:%i\n", blockIdx.y, blockIdx.x);
	printf("Block Size: Y:%i, X:%i\n", blockDim.y, blockDim.x);
	printf("Grid Size: Y:%i, X:%i\n", gridDim.y, gridDim.x);
	printf("==========================================\n");
*/

//	printf("Index: %i\n", index);

	//for(i = 1, a = 0; a < length; i++)
	//{

/*		if(IsCousin(index) == TRUE)
		{
			//printf("=Cousin %i\n", (threadIdx.y + 1) * threadIdx.x);

			values[index] = 1;
			//a++;

		}*/

	if(index == 1)
		return;

	for(i = 2; i < (index - 1); i++)
	{
		if(index % i == 0)
			return;
	}

	values[index] = index;

//	__syncthreads();
	//}
}

/*__global__ void  CousinCalculate(int Cousin, int *value)
{

}*/

int main(int argc, char *argv[])
{
	int i;

	cudaSetDevice(0);
	cudaSetDeviceFlags(cudaDeviceBlockingSync | cudaDeviceMapHost);
//	cudaSetDeviceFlags(cudaDeviceMapHost);

	/*if(cudaMalloc((void **) &gpuvalues, sizeof(int) * MAXVALUES) != cudaSuccess)
	{
		printf("Error allocating CUDA array\n");
		return -1;
	}*/

	if(cudaHostAlloc((void **) &cpuvalues, sizeof(int) * MAXVALUES, cudaHostAllocMapped) != cudaSuccess)
	{
		printf("Error allocating mappeable memory.\n");
		return -1;
	}


	for(i = 0; i < MAXVALUES; i++)
	{
		cpuvalues[i] = 0;
	}

	printf("A\n");

	if(cudaHostGetDevicePointer((void **)&gpuvalues, (void*) cpuvalues, 0) != cudaSuccess)
	{
		printf("Unable to map device memory.\n");
		return -1;
	}

	printf("Hola\n");

/*	if(cudaMemcpy(gpuvalues, cpuvalues, sizeof(int) * MAXVALUES, cudaMemcpyHostToDevice) != cudaSuccess)
	{
		printf("Error copying zero data\n");
                return -1;
	}*/


	/*if(cudaMalloc((void **) &var, sizeof(int)) != cudaSuccess)
	{
		printf("ERROR allocating magic number\n");
		return -1;
	}

	i = 0;

	if(cudaMemcpy(var, &i, sizeof(int), cudaMemcpyHostToDevice) != cudaSuccess)
	{
		printf("Error setting number\n");
		return -1;
	}*/

	printf("Executing kernel...\n");

	dim3 dimBlock(32,16);
	dim3 dimGrid(20,10);

	KernelCall<<<dimGrid,dimBlock>>>(gpuvalues);

	printf("Done1.\n");

	cudaThreadSynchronize();

	printf("Done2.\n");

/*	if(cudaMemcpy(cpuvalues, gpuvalues, sizeof(int) * MAXVALUES, cudaMemcpyDeviceToHost) != cudaSuccess)
	{
		printf("Error retrieving data\n");
		return -1;
	}*/

	for(i = 0; i < MAXVALUES; i++)
	{
		if(cpuvalues[i] != 0)
			printf("%i,", cpuvalues[i]);
	}


	cudaFree(cpuvalues);

	cudaThreadExit();

	return 0;
}
