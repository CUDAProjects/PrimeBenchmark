#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

int *cpuvalues;

#define MAXVALUES 786432

void CousinCheck(int *values)
{
	int index;
	int res;
	int i;


	for(index = 0; index < MAXVALUES; index++)
	{
		res = 0;

		for(i = 2; i < ((index + 2) - 1); i++)
		{
			if((index + 2) % i == 0)
			{
				res = 1;
				break;
			}
		}

		if(res == 0)
			values[index] = index + 2;
	}
}

int main(int argc, char *argv[])
{
	int i;
	GTimer *timer = g_timer_new();

	cpuvalues = (int *)malloc(sizeof(int) * MAXVALUES);

	if(cpuvalues == NULL)
	{
		printf("Error allocating host array\n");
		return -1;
	}

	for(i = 0; i < MAXVALUES; i++)
	{
		cpuvalues[i] = 0;
	}

	printf("Executing function...\n");

	g_timer_start(timer);

	CousinCheck(cpuvalues);

	g_timer_stop(timer);

	printf("Done.\n\n|");

	for(i = 0; i < MAXVALUES; i++)
	{
		if(cpuvalues[i] != 0)
			printf("%i|", cpuvalues[i]);
	}

	printf("\n\nFunction time (sec): %f\n",g_timer_elapsed(timer, NULL));

	g_timer_destroy(timer);
	free(cpuvalues);

	return 0;
}
