#include <stdio.h>
#include <cuda_runtime_api.h>
#include <glib.h>

__device__ int *gpuvalues;
int *cpuvalues;

#define MAXVALUES 73728

__global__ void CousinCheck(int *values)
{
	__device__ __shared__ int index;
	__device__ __shared__ int i;

	index = (((blockIdx.y * gridDim.x) + blockIdx.x) * (blockDim.x * blockDim.y)) + ((threadIdx.y * blockDim.x) + threadIdx.x);

	for(i = 2; i < ((index + 2) - 1); i++)
	{
		if((index + 2) % i == 0)
			return;
	}

	values[index] = index + 2;
}

int main(int argc, char *argv[])
{
	int i;
	GTimer *timer = g_timer_new();

	cudaSetDevice(0);
	cudaSetDeviceFlags(cudaDeviceBlockingSync);

	if(cudaMalloc((void **) &gpuvalues, sizeof(int) * MAXVALUES) != cudaSuccess)
	{
		printf("Error allocating CUDA array\n");
		return -1;
	}

	cpuvalues = (int *)malloc(sizeof(int) * MAXVALUES);

	if(cpuvalues == NULL)
	{
		printf("Error allocating host array\n");
		return -1;
	}

	for(i = 0; i < MAXVALUES; i++)
	{
		cpuvalues[i] = 0;
	}

	if(cudaMemcpy(gpuvalues, cpuvalues, sizeof(int) * MAXVALUES, cudaMemcpyHostToDevice) != cudaSuccess)
	{
		printf("Error copying zero data\n");
                return -1;
	}

	printf("Executing kernel...\n");

	dim3 dimBlock(3,3);
	dim3 dimGrid(128,64);

	g_timer_start(timer);

	CousinCheck<<<dimGrid,dimBlock>>>(gpuvalues);

	cudaThreadSynchronize();

	g_timer_stop(timer);

	printf("Done.\n\n");

	if(cudaMemcpy(cpuvalues, gpuvalues, sizeof(int) * MAXVALUES, cudaMemcpyDeviceToHost) != cudaSuccess)
	{
		printf("Error retrieving data\n");
		return -1;
	}

	printf("|");

	for(i = 0; i < MAXVALUES; i++)
	{
		if(cpuvalues[i] != 0)
			printf("%i|", cpuvalues[i]);
	}

	printf("\n\nKernel time (sec): %f\n",g_timer_elapsed(timer, NULL));

	g_timer_destroy(timer);
	cudaFree(gpuvalues);
	free(cpuvalues);

	cudaThreadExit();

	return 0;
}
