#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <glib.h>
#include <cuda_runtime_api.h>

//Type definitions
#define Number int

struct Range
{
	Number Beginning;
	Number End;
};

enum CousinProcessingType
{
	ProcessRange = 0,
	ProcessNumberOfCousins = 1
};

//Function declarations
__global__ void ProcessCousins(Number cousins, struct Range range, guint8 processingType, Number *processedCousins, Number *cousinArray);
__device__ guint8 IsCousin(Number number);

//Commandline variables
Number beginning = 0;
Number end = 0;
Number cousins = 0;
gboolean processcousins = FALSE;
gboolean processrange = FALSE;

//Commandline entries
GOptionEntry entries[] =
{
	{ "beginning", 'b', 0, G_OPTION_ARG_INT, &beginning, "Beggining of the range", "B" },
	{ "end", 'e', 0, G_OPTION_ARG_INT, &end, "End of the range", "E" },
	{ "cousins", 'c', 0, G_OPTION_ARG_INT, &cousins, "Cousin numbers to show", "C" },
	{ "processcousins", 'p', 0, G_OPTION_ARG_NONE, &processcousins, "Process cousin numbers", NULL },
	{ "processrange", 'r', 0, G_OPTION_ARG_NONE, &processrange, "Show cousin numbers between the range specified", NULL },
	{ NULL }
};

//CUDA-specific code
__device__ Number *device_cousins;
Number *host_cousins;
Number *host_processed_cousins;
__device__ Number *device_processed_cousins;

/* Main program entry */
int main(int argc, char *argv[])
{
	cudaSetDevice(0);

	GOptionContext *context;

	context = g_option_context_new("- process cousins numbers using CUDA :D");

	g_option_context_add_main_entries (context, entries, NULL);
	g_option_context_set_help_enabled(context, TRUE);

	if(g_option_context_parse(context, &argc, &argv, NULL) == TRUE)
	{
		struct Range range;

		if(((processrange == TRUE) && (processcousins == TRUE)) || ((processrange == FALSE) && (processcousins == FALSE)))
                {
                        printf("Please, choose a valid cousin processing type.\n");
			return -1;
                }

		if(((beginning < 1) || (end <= beginning)) && (processrange == TRUE))
		{
			printf("Please, choose a valid cousin range.\n");
			return -1;
		}

		if((cousins <= 0) && (processcousins == TRUE))
		{
			printf("Please, choose a valid cousins value.\n");
			return -1;
		}

		if(processrange == TRUE)
		{
			range.Beginning = beginning;
			range.End = end;

			//printf("\033[1;34m<<<\033[1;33mList of Cousin numbers (here we go!! XD)\033[1;34m>>>\033[0m\n\n");

			//printf("\033[1;31mNumber of cousins obtained:\033[1;37m %li\033[0m\n\n", ProcessCousins(0, range, ProcessRange));

			//printf("\033[1;31m\n\nList finished!! :P\033[0m\n\n");

			printf("Function not implemented");
		}
		else
		{
			printf("Creating CUDA arrays...\n");

			if(cudaMalloc((void **) &device_cousins, sizeof(Number) * cousins) != cudaSuccess)
			{
				printf("Error allocating CUDA arrays.\n");
				return -1;
			}

			if(cudaMalloc((void **) &device_processed_cousins, sizeof(Number)) != cudaSuccess)
			{
				printf("Error allocating CUDA number.\n");
				return -1;
			}

			printf("Done.\n");

			printf("Creating system arrays...\n");

			/*if(cudaMallocHost((void **)&host_cousins, sizeof(Number) * cousins) != cudaSuccess)
			{
				printf("Error allocating Host arrays.\n");
				return -1;
			}*/

			host_cousins = (Number *)malloc(sizeof(Number) * cousins);

			if(host_cousins == NULL)
			{
				printf("ERRROE\n");
				return -1;
			}

			Number d;

			printf("Creating empty zeros\n");

			for(d = 0; d < cousins; d++)
			{
				host_cousins[d] = 0;
			}

			if(cudaMemcpy(device_cousins, host_cousins, sizeof(Number) * cousins, cudaMemcpyHostToDevice) != cudaSuccess)
			{
				printf("Failed to copy empty zeros on device\n");
				return -1;
			}

	
/*
			if(cudaMallocHost((void **)&host_processed_cousins, sizeof(Number)) != cudaSuccess)
			{
				printf("Error allocatng Host number.\n");
				return -1;
			}*/

			host_processed_cousins = (int *)malloc(sizeof(int));

			if(host_processed_cousins == NULL)
			{
				printf("ERROR\n");
				return -1;
			}

			//cudaThreadSynchronize();

			//printf("Done.\n");

			ProcessCousins<<<1,1>>>(cousins, range, ProcessNumberOfCousins, device_processed_cousins, device_cousins);

//			cudaThreadSynchronize();
//			sleep(10);

			//printf("Copying %i cousins...\n", cousins);

			if(cudaMemcpy(host_cousins, device_cousins, sizeof(Number) * cousins, cudaMemcpyDeviceToHost) != cudaSuccess)
			{
				printf("Error retrieving data array.\n");
				return -1;
			}

/*			if(cudaMemcpy(host_processed_cousins, device_processed_cousins, sizeof(Number), 
cudaMemcpyDeviceToHost) != cudaSuccess)
			{
				printf("Error retrieving processed.\n");
				return -1;
			}*/

			//printf("%s\n", cudaGetErrorString(cudaGetLastError()));

			//printf("Processed %li cousins!!\n", *host_processed_cousins);

			for(d = 0; d < cousins; d++)
			{
				printf("%i,", host_cousins[d]);
			}

			//printf("\033[1;31mNumber of cousins obtained:\033[1;37m %li\033[0m\n\n", ProcessCousins(cousins, range, ProcessNumberOfCousins));
		}
	}
	else
	{
		printf("%s", g_option_context_get_help(context, TRUE, NULL));
	}

	return 0;
}

/* Process the cousins specified */
__global__ void ProcessCousins(Number cousins, struct Range range, guint8 processingType, Number *processedCousins, Number *cousinArray)
{
	Number i;
	Number a;

//	Number c;
//	Number res;

	/*switch(proc)
	{
		case ProcessRange:
			for(i = (threadIdx.x) * 20, a = 0; i < range.End; i++)
        		{
                		if(IsCousin(i) == TRUE)
				{
					cousinArray[a] = i;
                        		a++;
				}
			}
			break;
		case ProcessNumberOfCousins:*/
			for(i = 0, a = 0; a < cousins; i++)
			{
				if(IsCousin(i) == TRUE)
				{
					cousinArray[a] = i;
					a++;
				}
					//cousinArray[a] = 1;
					//a++;
			}
//					__syncthreads();
			//	}


/*        			if(i == 1)
			                continue;

				res = 0;

			        for(c = 2; c < (i - 1); c++)
			        {
			                if((i % c) == 0)
			                        res = 1;
			        }

				if(res == 0)
				{
					cousinArray[a] = i;
					a++;
				}
				//cousinArray[a] = 0;
				//a++;
			}*/
//			break;
//	}

	*processedCousins = a;
}

/* Checks if a number is cousin */
__device__ guint8 IsCousin(Number number)
{
	Number i;

	if(number == 1)
		return FALSE;

	for(i = 2; i < (number - 1); i++)
	{
		if((number % i) == 0)
			return FALSE;
	}

	return TRUE;
}
